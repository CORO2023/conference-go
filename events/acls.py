from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json
import logging

logging.basicConfig(level=logging.DEBUG)

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    payload = {"query": f'{city} {state}'}
    url = 'https://api.pexels.com/v1/search/'
    response = requests.get(url, headers=headers, params=payload)
    images = json.loads(response.content)
    picture = images.get("photos", [{}])[0].get("src", {}).get("original")
    return {"picture_url": picture}


def get_lat_long(location):
    base_url = 'http://api.openweathermap.org/geo/1.0/direct'
    params = {
        'q': f"{location.city},{location.state.abbreviation}, USA",
        'appid': OPEN_WEATHER_API_KEY
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    logging.debug(parsed_json) # Print to the log to figure out whats in the dictionary ...
    latitude = parsed_json[0].get("lat")
    longitude = parsed_json[0].get("lon")
    return {"latitude": latitude, "longitude": longitude}


def get_weather_data(location):
    lat_long = get_lat_long(location)
    base_url = "http://api.openweathermap.org/data/2.5/weather"
    params = {
        'lat': lat_long.get("latitude"),
        'lon': lat_long.get("longitude"),
        'appid': OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }

    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    temp = parsed_json["main"].get("temp")
    description = parsed_json["weather"][0].get("description")
    weather_data = {"temp": temp, "description": description}

    return weather_data
